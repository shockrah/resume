---
company: Amazon
when: May 2020 - Dec 2020
title: Fullfilment Associate
rank: -1
---

Fulfill basic warehouse and stock order duties. Responsibilities include
offloading merchandise trucks, locating stock, and packing items for shipment.

