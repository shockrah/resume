---
company: California State University Monterey Bay
when: Sep 2019 - Dec 2019
title: Capstone Project Lead
rank: 1
---

Led development of data analytics toolset with a team of
4 members. Developed custom data visualization tools using:

* Python
* Numpy
* Matplotlib

Worked with data from the U.S Naval Research Laboratory’s 
compute clusters.
