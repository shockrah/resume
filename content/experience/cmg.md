---
company: Career Mentor Group
when: May 2021 - January 2022
title: Devops Consultant
rank: -3
---

Led a project to develop fully automated infrastructure deployment pipeline
alongside a DevSecOps pipeline leveraging tools such as Nessus and OpenRMF to
achieve DoD network security compliance on an AWS VPC.
 
Integrated web scrapers with automated CI/CD data analysis
pipelines contributing thousands of data points to a much
larger dataset. Designed and built cloud infrastructure leveraging automation tools 
including:

* **Python & Pandas** for helping build and analyze the dataset constructed

* **Terraform** for constructing components of the infrastructure

* **Ansible** to keep applications and services properly maintained

* **Gitlab CI/CD** for application building, testing, and deployment

_Started as intern as Critical Design Associates(sister company) before 
moving into this role._
