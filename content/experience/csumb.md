---
title: Computer Architecture Teaching Assistant
company: CSU Monterey Bay
when: Oct 2019 - Dec 2019
rank: 0
---

Prepared students for exams and helped with assignments through consistent
office hour scheduling and by providing ample feedback on assignments.
Course material covered MIPS assembly , sequential and combinatorial logic,
and an introduction to CPU design. Left because I was graduating that very
semester that I worked.
