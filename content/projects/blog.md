---
link: https://gitlab.com/shockrah/shockrah-city
img: /images/blog.png
title: Personal Static Blog
slogan: Powered By Hugo and Gitlab CI/CD
---

My own personal blog where I post about projects that I'm working on
at the time. I keep the site maintained through the use of automated
CI/CD pipelines with Gitlab and Hugo. Built a responsive site theme
which leverages Go templates producing a lightweight front-end.

Tools used:

* Gitlab CI/CD
* Go templates
* Ansible
* HTML
* CSS
* Javascript

Live website can be found here: [shockrah.xyz](https://shockrah.xyz)
