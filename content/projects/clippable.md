+++
link="https://shockrah.gitlab.io/clippable" 
img="https://shockrah.gitlab.io/clippable/images/og-bg.jpg" 
title="Clippable"
slogan="A self hosted video streaming platform"
+++

Designed and architected a cost effective scalable video streaming
platform. Implemented deployment automation systems which reduce
standard server maintainence to a single command. Reduced operational
costs by evaluating optimal EC2 instance for the given network load.


Used the following tools:
* Gitlab
* Ansible
* Rust
* Tera Templating Engine
* Typescript/Node
* AWS
* Terraform

**[Live project instance](https://clips.shockrah.xyz)**
