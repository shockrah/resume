+++
link="https://gitlab.com/shockrah/freechat" 
img="/images/freechat-banner.png" 
title="Freechat"
slogan="FOSS Decentralized Chatting"
+++

Developed a scalable decentralized chat platform. Designed and documented 
asynchronous API protocol built with Rust, Python, and containerized all
parts of the system for simple deployment. Also drafted a wiki page
detailing all available endpoints which can be found [here](https://freechat.shockrah.xyz)

Built with the following tools:

* **Rust** for the backend REST API and _RTC Notification Service_

* **Python** for full automated API testing

* **Gitlab CI/CD** for integration testing and continuous deployment
of containers


