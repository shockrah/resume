---
link: https://gitlab.com/shockrah/jankos
img: https://gitlab.com/uploads/-/system/project/avatar/9825109/jos.png?width=64
title: Handmade Operating System
slogan: An x86 kernel I wrote for fun and learning
---

Designed and implemented a baremetal x86 kernel written completely
from scratch. ISO files are automatically built via Gitlab CI/CD
pipelines which allow users to run the kernel on real hardware!

Tools and software used included:

* C
* x86 Assembly
* Cutter
* Bochs
* _No standard library & and no external libraries were used_

