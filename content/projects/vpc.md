---
link: https://gitlab.com/shockrah/project-athens
img: /images/projectathens.png
title: Virtual Private Cloud Infrastructure
slogan: Architecture hosting all my web facing projects
---

Infrastructure code for my public public web-facing projects.
Designed to accommodate containerized applications, static
web servers, and community chat bot's. Development streamed on
Twitch and is an on-going project as I continue to support my
communities.

Tools used:

* Terraform 
* Ansible
* Bash
* Linux

Architecture documentation can be found [here](https://gitlab.com/shockrah/project-athens/-/blob/master/readme.md)
