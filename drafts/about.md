---
title: About me
---

Musical internet sweet-tooth contributing to the FOSS community.

When I'm not contributing to data analytics or infrastructure development at
Career Mentor Group, I spend time (literally) wandering forests, making candy,
or building out something for the FOSS community.

If ever you're wondering what it is I'm working on just look at my Gitlab
profile or check my personal blog where I usually post my own thoughts
on things that I've been working on.

