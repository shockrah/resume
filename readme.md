# Source for online resume

Here are the configuration files that go into building my online resume page, hosted [here](https://shockrah.gitlab.io/resume/).

If you have any questions feel free to email me at `alejandros714@protonmail.com`.


## Building

Requires Hugo version: `0.89-extended`

Theme is built into the repo so no extra deps are required.

## Configuring

### Ordering By File `rank`

> The following only applies to markdown that take a `rank` parameter.

The markdown file with the smallest `rank` is listed _first_,
while the markdown with the largest `rank` is listed _last_.
For this reason I personally label things from `-1` to `-n`
since it makes it _visually_ easier to understand when reading.

### Projects

These do not have any kind of order so whatever Hugo normally
uses to sort pages is what is used.


Each entry takes the following header parameters:

* link   : `plain string` Link to project
* img    : `plain string(url)` URL target for the image
* title  : `plain string` Title/name of the project
* slogan : `plain string` Short 1-liner to desribe the project


### Experience

Each entry in experience is its own markdown file the header
should contain the following[name: `type`]:

* company : `plain string`
* when    : `plain string`
* title   : `plain string`
* rank    : `number`


### Education

Each file entry in the education folder requires the following fields:

* institution  : `plain string`
* when         : `plain string`
* degree       : `plain string`
* rank         : `number`
